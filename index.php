<!DOCTYPE html>
<html lang="ru">
<head>
	<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
	<meta name="description" content="АС Компонент - поставка электроборудования">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>АС Компонент - поставка электроборудования</title>
	<link rel="icon" type="image/png" href="favicon.png">
    <!-- bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- ./bootstrap -->
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<div class="main">
		<!--header-->
        <header>
            <!--main menu-->
            <div class="main-menu">
                <nav class="navbar">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="/" class="navbar-brand">
                            	<img src="img/as-component-logo.png" alt="Logo">
                            </a>
                            <button type="button" data-toggle="collapse" data-target="#navbar" class="navbar-toggle collapsed">
                            	<span class="sr-only">Toggle navigation</span>
                            	<span class="icon-bar"></span>
                            	<span class="icon-bar"></span>
                            	<span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="navbar" class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-left uppercase" id="yw0">
								<li class="hidden-xs"><a href="#about-us">О компании</a></li>
								<li><a href="#decisions">Решения</a></li>
								<li><a href="#map">Контакты</a></li>
							</ul>                                 
                        </div>
                        <div class="contact-phones hidden-xs">
                        	<ul>
                        		<li><a href="tel:+74993906981">+7 (499) 390 69 81</a></li>
                        		<li><a href="tel:+79296734857">+7 (929) 673 48 57</a></li>
                        	</ul>
                        </div>
                    </div>
                </nav>
            </div>
            <!--./main menu-->
        </header>
        <!--./header-->
        <section class="banner-main-section">
	        <div class="jumbotron">
				<div class="container">
					<div class="row">
						<div class="col-md-6 hidden-xs"></div>
						<div class="col-md-6 col-xs 12 banner-content">
							<h2>Поставка <br> электрооборудования</h2>
							<p>Надёжный поставщик <br> правильных решений!</p>
							<!--Modal toggle-->
                            <button class="modal-toggle" data-toggle="modal" data-target="#modalWidget">
                                оставить заявку
                            </button>
                            <!--
                            <button class="modal-toggle" data-toggle="modal" data-target="#modalSuccess">
                                показать success
                            </button>
                        	-->
                            <!--./Modal toggle-->
                            <!--Modal body-->
                            <div class="modal fade" id="modalWidget" tabindex="-1" role="dialog" aria-labelledby="modalWidgetLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body text-center">
                                            <h3>оставить заявку</h3>
                                            <button type="button" class="btn btn-default close-button" data-dismiss="modal"></button>
                                            <form name="customer-application" action="mailer-handler.php" method="post">
                                            	<input type="text" name="name" placeholder="ФИО" required>
                                            	<input type="text" name="contact" placeholder="Контакты" required>
                                            	<input type="email" name="email" placeholder="Email" required>
                                            	<textarea name="textarea" cols="34" rows="5" placeholder="Тест сообщения"></textarea>
                                            	<div class="text-left">
                                            		<input type="checkbox" name="agreement" id="agreement" required/>
                                            		<label for="agreement">Я даю согласие на передачу и <br> обработку персональных данных</label>
                                            	</div>
                                            	<input type="submit" value="оставить заявку">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade success" id="modalSuccess" tabindex="-1" role="dialog" aria-labelledby="modalSuccessLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body text-center">
                                            <h4>Ваша заявка принята</h4>
                                            <button type="button" class="btn btn-default close-button" data-dismiss="modal"></button>
											<p>Ожидайте звонка <br> специалиста</p>
                                            <input type="button" data-dismiss="modal" value="на главную">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--./Modal body-->
						</div>
					</div>
				</div>
			</div> 
        </section>
		<section id="decisions" class="text-section decisions">
			<div class="jumbotron">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2>Решения</h2>
						</div>
						<!--decisions desktop version-->
						<div class="col-md-4 tile engineering hidden-xs">
							<img src="img/tiles/tile-engineering.jpg" alt="Машиностроение">
							<div class="tile-text-wrapper text-left">
								<h3>Машиностроение</h3>
							</div>
						</div>
						<div class="col-md-4 tile railway-transport text-center hidden-xs">
							<img src="img/tiles/tile-railway-transport.jpg" alt="Железнодорожный транспорт">
							<div class="tile-text-wrapper text-left">
								<h3>Железнодорожный транспорт</h3>
							</div>
						</div>
						<div class="col-md-4 tile steel-tapes-tools text-right hidden-xs">
							<img src="img/tiles/tile-steel-tapes-tools.jpg" alt="Инструменты для натяжения и резки стальных лент">
							<div class="tile-text-wrapper text-left">
								<h3>Инструмент <br> для натяжения <br> и резки <br> стальных лент</h3>
							</div>
						</div>
						<div class="col-md-4 tile nku-produsing hidden-xs">
							<img src="img/tiles/tile-nku-produsing.jpg" alt="НКУ строение">
							<div class="tile-text-wrapper text-left">
								<h3>НКУ строение</h3>
							</div>
						</div>
						<div class="col-md-4 tile automation text-center hidden-xs">
							<img src="img/tiles/tile-automation.jpg" alt="АСУТП, автоматика, электроника">
							<div class="tile-text-wrapper text-left">
								<h3>АСУТП, <br> автоматика, <br> электроника</h3>
							</div>
						</div>
						<div class="col-md-4 tile kru-produsing text-right hidden-xs">
							<img src="img/tiles/tile-kru-produsing.jpg" alt="КРУ строение">
							<div class="tile-text-wrapper text-left">
								<h3>КРУ строение</h3>
							</div>
						</div>
						<!--./decisions desktop version-->
					</div>
				</div>
			</div>
		</section>
		<section class="text-section decisions visible-xs">
			<!--decisions mobile version-->
			<div class="fifty-percent-width-trick float-left-trick tile engineering text-center visible-xs">
				<img src="img/tiles/tile-engineering-mobile.jpg" alt="Машиностроение">
				<div class="tile-text-wrapper text-center">
					<h3>Машиностроение</h3>
				</div>
			</div>
			<div class="fifty-percent-width-trick float-right-trick tile railway-transport text-center visible-xs">
				<img src="img/tiles/tile-railway-transport-mobile.jpg" alt="Железнодорожный транспорт">
				<div class="tile-text-wrapper text-center">
					<h3>Железнодорожный транспорт</h3>
				</div>
			</div>
			<div class="fifty-percent-width-trick float-left-trick tile steel-tapes-tools text-center visible-xs">
				<img src="img/tiles/tile-steel-tapes-tools-mobile.jpg" alt="Инструменты для натяжения и резки стальных лент">
				<div class="tile-text-wrapper text-center">
					<h3>Инструменты <br> для натяжения <br> и резки <br> стальных лент</h3>
				</div>
			</div>
			<div class="fifty-percent-width-trick float-right-trick tile nku-produsing text-center visible-xs">
				<img src="img/tiles/tile-nku-produsing-mobile.jpg" alt="НКУ строение">
				<div class="tile-text-wrapper text-center">
					<h3>НКУ строение</h3>
				</div>
			</div>
			<div class="fifty-percent-width-trick float-left-trick tile automation text-center visible-xs">
				<img src="img/tiles/tile-automation-mobile.jpg" alt="АСУТП, автоматика, электроника">
				<div class="tile-text-wrapper text-center">
					<h3>АСУТП, <br> автоматика, <br> электроника</h3>
				</div>
			</div>
			<div class="fifty-percent-width-trick float-right-trick tile kru-produsing text-center visible-xs">
				<img src="img/tiles/tile-kru-produsing-mobile.jpg" alt="КРУ строение">
				<div class="tile-text-wrapper text-center">
					<h3>КРУ строение</h3>
				</div>
			</div>
			<div class="clear-both-trick"></div>
			<!--./decisions mobile version-->
		</section>
		<section class="text-section advantages">
			<div class="jumbotron">
				<div class="container">
					<div class="row">
						<div class="col-md-12 visible-xs">
							<h2>Преимущества</h2>
						</div>
						<!--advantages desktop version-->
						<div class="col-md-4 certificate text-center hidden-xs">
							<img src="img/advantages/certificate.png" alt="Сертификаты"><br>
							<span>Все сертификаты и паспорта <br> предоставляются с товаром.</span>
						</div>
						<div class="col-md-4 delivery text-center hidden-xs">
							<img src="img/advantages/delivery.png" alt="Доставка"><br>
							<span>Бесплатная доставка <br> до Ваших дверей.</span>
						</div>
						<div class="col-md-4 world-wide text-center hidden-xs">
							<img src="img/advantages/world-wide.png" alt="Со всего мира"><br>
							<span>Поставка оборудования <br> импортного и отечественного <br> производства.</span>
						</div>
						<!--./advantages desktop version-->
						<!--advantages mobile version-->
						<div class="col-md-4 certificate text-center visible-xs">
							<img src="img/advantages/certificate-MOBILE.png" alt="Сертификаты"><br>
							<span>К товару прилагаются <br> все сертификаты <br> и паспорта.</span>
						</div>
						<div class="col-md-4 delivery text-center visible-xs">
							<img src="img/advantages/delivery-MOBILE.png" alt="Доставка"><br>
							<span>Бесплатная доставка <br> до Ваших дверей.</span>
						</div>
						<div class="col-md-4 world-wide text-center visible-xs">
							<img src="img/advantages/world-wide-MOBILE.png" alt="Со всего мира"><br>
							<span>Поставка импортного <br> и отечественного <br> оборудования.</span>
						</div>
						<!--./advantages mobile version-->
					</div>
				</div>
			</div>
		</section>
		<section id="about-us" class="text-section about-us hidden-xs">
			<div class="jumbotron">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2>О нас</h2>
						</div>
						<div class="col-md-4 text-center">
							<img src="img/certificate.jpeg" alt="Certificate">
							<p>Сертификат официального <br> дистрибьютера Компании ЭСА</p>
						</div>
						<div class="col-md-8">
							<p>АС Компонент быстро развивающаяся компания, осуществляющая  <br> комплексные  поставки импортного и отечественного электрооборудования, <br> а также инструмент  для натяжения и резки стальных лент.</p>
							<p>Цель компании -  эффективные и долгосрочные взаимоотношения <br> с заказчиками и партнерами. Благодаря сотрудничеству с мировыми <br> производителями и крупнейшими дистрибьюторами электротехнической <br> продукции и электронных компонентов, мы предоставляем нашим клиентам <br> гибкие ценовые условия и решаем задачи по оптимизации производства.</p>
							<p>Главные приоритеты - качество и короткие сроки получения продукции.</p>
							<p>АС Компонент – надежный поставщик правильных решений!</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="map" class="text-section map">
			<div class="jumbotron">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2>Мы находимся</h2>
						</div>
						<div class="col-md-4 hidden-xs">
							<p>Адрес офиса: <br> 125466, г. Москва <br> ул. Юровская, д.92 <br> помещение 1, комн.40</p>
							<p>
								<a href="tel:+74993906981">+7 (499) 390 69 81</a>
								<br>
								<a href="tel:+79296734857">+7 (929) 673 48 57</a>
							</p>
							<p><a href="mailto:info@as-component.ru">Email: info@as-component.ru</a></p>
						<!--	<p><a href="mailto:sale@as-comp.ru">Email - приём заявок</a></p>	-->
						</div>
						<div class="col-md-4 visible-xs text-center">
							<p>+7(499)3906981 <br> +7(929)6734857</p>
							<p>Адрес офиса: <br> 125466, г. Москва <br> ул. Юровская, д.92 <br> помещение 1, комн.40</p>
						</div>
						<div class="col-md-8 text-center hidden-xs">
						<!--	<img src="img/map.png" alt="Map">	-->
							<iframe src="https://yandex.by/map-widget/v1/-/CBFHUKSntD" width="100%" height="400" frameborder="1" allowfullscreen="true"></iframe>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="image-section map visible-xs">
			<iframe src="https://yandex.by/map-widget/v1/-/CBFHUKSntD" width="100%" height="400" frameborder="1" allowfullscreen="true"></iframe>
		</section>
		<section class="solid-blue-line"></section>
	</div>
	<!--footer-->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<img src="img/as-component-logo.png" alt="Logo"> <br>
					<span>&copy; Все права защищены</span>
				</div>
			</div>
		</div>
	</footer>
	<!--./footer-->
	<!-- jquery -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
	<!-- ./jquery -->
	<!-- js bootstrap -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!-- js bootstrap -->
	<!-- modalsuccess message toggle-->
	<?php 
		if(!empty($_GET["modalsuccess"])) {
			?>
			<script type="text/javascript">
			    $(window).on('load',function(){
			        $('#modalSuccess').modal('show');
			    });

			    $("#modalSuccess").on('hide.bs.modal', function () {
			        window.location.replace("http://www.as-component.ru/");
			    });
			</script>
			<?
		}
	?>
	<!-- ./modalsuccess message toggle-->
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>